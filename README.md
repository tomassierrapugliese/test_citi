# Evaluación Técnica Citi

Tomas Sierra Pugliese

**Implementaciones en java ejercicios 1 al 8 en carpeta src**

# Base de datos

**¿Qué es un “primary key”? ¿Qué es un “foreign key”?**

`Una “primary key” es un campo que identifica inequívocamente a un registro en una tabla y una “foreign key” es una primary key de otra tabla a la que se hace referencia.`


**¿Cuál es el error en la siguiente sentencia? SELECT Pais, Ciudad, SUM(NumeroHabitantes) FROM Lugar GROUP BY Ciudad**

`La query debería estar agrupada por País.`

**¿Qué es un trigger? ¿Cuáles son los tres tipos básicos de trigger?**

```
Es un procedimiento que se ejecuta ante un evento determinado en una tabla
tipos: 
DML, para sentencias como INSERT, UPDATE o DELETE 
DDL para sentencias como CREATE, DROP, ALTER
Logon para los inicios de sesión.
```


**¿Qué es una transacción? ¿Para que se usa?**

```
Es una o un conjunto de operaciones que se agrupan como una sola, debiéndose ejecutar todas ellas para finalizar la misma y en 
caso de que alguna no lo haga u ocurra un error, se cancela la transacción, esto tiene la finalidad
de mantener la consistencia de los datos en una base.
```


**¿Qué es un índice? ¿Para que se usa?**

`Es una estructura de datos que hace referencia a columna de una o más tablas, si bien es redundante, mejora el rendimiento de las consultas.`

**¿Cuál es la diferencia entre los siguientes fragmentos?**

```
... WHERE Id NOT IN (SELECT Id FROM Tabla2)
... WHERE NOT EXISTS (SELECT NULL FROM Tabla2 t2 WHERE t2.Id = t1.Id)
```

`Si hubiese algún campo nulo, el operador NOT IN no devolvería ningún valor, en cambio NOT EXISTS funcionaria correctamente.`


**1. Nombre de los productos ordenados por mayor cantidad total de venta (más vendido primero)**

```
SELECT PRODUCTO.Nombre
FROM PRODUCTO, VENTA
WHERE VENTA.IdProducto = PRODUCTO.IdProducto 
GROUP BY VENTA.Cantidad
ORDER BY VENTA.Cantidad DESC
```


**2. Nombre de producto y precio promedio de venta (ponderado por cantidad)**

```
SELECT PRODUCTO.Nombre, VENTA.Precio/VENTA.Cantidad as PromedioVenta 
FROM PRODUCTO, VENTA 
WHERE VENTA.IdProducto = PRODUCTO.idproducto 
GROUP BY VENTA.Cantidad 
ORDER BY VENTA.Cantidad DESC; 
```


**3. Nombre de producto y cantidad vendida en el 2006 con precio promedio de venta (en 2006) mayor a $10**

```
SELECT PRODUCTO.Nombre, VENTA.Cantidad
FROM PRODUCTO, VENTA
WHERE VENTA.IdProducto = PRODUCTO.IdProducto 
AND VENTA.Fecha BETWEEN '2006-01-01' AND '2006-12-12'
GROUP BY VENTA.Cantidad
HAVING VENTA.Precio/VENTA.Cantidad > 10
ORDER BY VENTA.Cantidad desc;
```




# Linux

**1) ¿Con qué comando se averigua el directorio actual?**

 `pwd`
 
**2) ¿A qué directorio espera que se cambie si se ejecuta “cd” sin argumentos?**

`al de inicio`

**3) ¿Cómo se obtiene un listado extendido de archivos (con tamaño, fecha etc.)?**

`ls -l`

**4) ¿Qué es un proceso? ¿Cómo obtengo una lista de todos los procesos de mi usuario?**

 `Es un programa en ejecución y se obtiene una lista todos ellos con el comando “ps”`

**5) Dada la siguiente salida:**

```
> /bin/ls -a
 .. .harry Hat boat hill house push tree
```

**¿Qué imprime el comando “echo h***

```
imprime el nombre de los archivos que comiencen con “h” discriminando el case.
hill house
```


**6) Estando en el mismo directorio que en la pregunta 5 ¿Qué valor tendría “$1” si se llama al siguiente**
**script con “h*” como argumento?**

```
'#/bin/ksh'

echo my first argument is $1
```


```
a) h*
b) .harry
c) hill
d) Hat
e) ninguno de los anteriores (x)
```



**7) ¿Qué ocurre si por accidente ingresa un espacio extra al querer borrar todos los archivos con extensión “.old” y escribe “rm -f * .old”?**

`Se borran todos los archivos del directorio`

**8) Dada las siguientes salidas:**

```
> ls -l
total 0
-rw-r--r-- 1 user users 38 Oct 6 12:24 myscript.sh
> cat myscript.sh
!/bin/ksh
echo This is a test script
> ./myscript.sh
ksh: ./myscript.sh: cannot execute - Permission denied
```

**¿Qué debería hacer para solucionar el problema y poder ejecutar el script?**

Se le deben otorgar permisos de ejecución al archivo.

`ej: chmod 700 myscript.sh`

**9) Para el mismo archivo de la pregunta 8 ¿Qué debo hacer para que pueda ser modificado por cualquier**
**usuario del grupo “developers”? (sin importar el acceso para el grupo “users”)**

`chmod 777`

**10) ¿Cómo puedo saber cuanto espacio en disco ocupa un directorio completo?**

`du -h nombreDirectorio`

**11) ¿Cómo puedo saber cuanto espacio libre hay en un volumen dado?**

`df -h`

**12) ¿Cómo puedo saber qué otros usuarios están conectados al sistema?**

`“w” o “who” o también “users”`

**13) ¿Qué comando ejecutaría (con parámetros) para hacer un backup comprimido del directorio “old” y todos sus subdirectorios?**

`tar -cf backup.tar old`

**14) ¿Cómo haría para recuperar todos los archivos contenidos en un archivo llamado “backup.tgz”?**

 `tar -xf backup.tgz -C /temp`

**15) ¿Con qué comando puedo buscar ocurrencias de un texto dentro de un archivo?**

 `con el comando grep.`

**Escriba como buscaría las ocurrencias de “myMethod” dentro de todos los archivos con extensión “.cpp” en el directorio actual.**

`grep ‘myMethod’ *.cpp`

**16) Elija uno de los comandos “cut”, “sed”, “awk” y explique para que se usa.**

```
cut es un comando utilizado para cortar o filtrar columnas de uno o mas ficheros, indicando a partir de qué caracter, qué cantidad de columnas 
y pudiendo sustituir caracteres para mejorar la presentacion del texto.
al archivo empleados.txt :

2020-01-01,Damian,Gomez,Argentina
2019-03-11,Martina,Milone,Argentina

le aplicamos cut -d "," -f 2,3 | tr -s ':' ' 'y quedaría:

Damian Gomez
Martina Milone
```

**17) ¿Que es un link simbólico? Escriba como crearía un link simbólico llamado “alink.txt” a un file**
**llamado “afile.txt”**

```
Es un acceso directo a un archivo o carpeta.
ln -s afile.txt alink.txt
```


**18) ¿Cómo se ejecutan aplicaciones gráficas en un servidor remoto usando Unix/Linux?**

```
Logueando mediante protocolo ssh sumando el parámetro -X.
ej: ssh ssh -X usuario@nombrehost
```


**19) Explique que debe contener la variable “DISPLAY” y para qué sirve.**

```
consta de 3 partes: host : cantidadpantallas : nropantalla
sirve para indicarle al cliente a que servidor debe conectarse
```



**20) ¿Cómo se redirige la entrada/salida/error estándar y para qué sirve? ¿Qué es un pipe?**

```
Sirve para transformar la información de un tipo a otro.

ej  ls -a > lista.txt  se guarda el resultado del comando en un archivo de texto
     cp archivo1 archivo2 2> error.txt  los errores se guardan en un archivo de texto
     wc < archivo.txt el comando wc recibe como parametro el archivo.txt

El pipe s un redireccionamiento utilizado cuando la salida de un comando, es la entrada de otro
ej: cat archivo.txt | wc 
```


**21) ¿Qué son las señales? ¿Cómo se envía una señal a un proceso y en qué casos sería útil/necesario?**
```
 Las señales son avisos que un proceso le envía a otro proceso y el sistema operativo se encarga de que quien la recibió la trate inmediatamente
 mediante un procedimiento definido para dicho aviso. Se pueden enviar señales a los procesos mediante la terminal, un ejemplo 
 muy útil es el comando “kill” al cual le indicamos el pid de los procesos a los cuales queremos finalizar, esto dispara 
 el envío de la señal TERM por parte del sistema operativo al proceso indicado.
```




