package com.citi.test.java;

public class Ejercicio4 {

	public static void main(String[] args) {
		System.out.println(es_palindromo("NEUQUEN"));
		System.out.println(es_palindromo("MISIONES"));
		System.out.println(es_palindromo("racecar"));
	}
	
	public static boolean es_palindromo(String s) {
		
		if (s == null || s.length() <= 1 ) {
			return false;
		}
		
		boolean esLongitudPar = s.length()%2 == 0 ? true : false;
		boolean resultado = false;
		int indiceMedio = s.length()/2;

		if(esLongitudPar) {
			resultado = expandirDesde(s, indiceMedio, indiceMedio + 1);
		} else {
			resultado = expandirDesde(s, indiceMedio, indiceMedio);
		}
		
		return resultado;
	}
	
	public static boolean expandirDesde(String s, int izquierda, int derecha) {
		
		if (s == null || izquierda > derecha) {
			return false;
		}
		
		while (izquierda > 0  && derecha < s.length() && s.charAt(izquierda) == s.charAt(derecha)) {
			izquierda--;
			derecha++;
		}
		return (derecha + izquierda) + 1 == s.length();
	}
	
}
