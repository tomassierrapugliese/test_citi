package com.citi.test.java;

public class Ejercicio5 {

	public static void main(String[] args) {
		ABO a = new ABO();
		Nodo raiz = null;
		
		raiz = a.insertar(raiz, 1);
		raiz = a.insertar(raiz, 6);
		raiz = a.insertar(raiz, 7);
		raiz = a.insertar(raiz, 9);
		raiz = a.insertar(raiz, 17);
		raiz = a.insertar(raiz, 21);

		a.mostrarEnOrden(raiz);
	}

	static class ABO {
		Nodo raiz;

		public void mostrarEnOrden(Nodo nodo) {
			if (nodo == null) {
				return;
			}

			mostrarEnOrden(nodo.izquierda);
			System.out.print(nodo.valor + " ");
			mostrarEnOrden(nodo.derecha);
		}

		public Nodo insertar(Nodo actual, int val) {
			if (actual == null) {
				return nuevoNodo(val);
			}

			if (val < actual.valor) {
				actual.izquierda = insertar(actual.izquierda, val);
			} else if ((val > actual.valor)) {
				actual.derecha = insertar(actual.derecha, val);
			} else {
				// ya existe
			}
			
			return 	actual;
		}

		public Nodo nuevoNodo(int k) {
			Nodo a = new Nodo();
			a.valor = k;
			a.izquierda = null;
			a.derecha = null;
			return a;
		}
	}
	
	static class Nodo {
		Nodo izquierda;
		Nodo derecha;
		int valor;
	}
}
