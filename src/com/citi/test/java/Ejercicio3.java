package com.citi.test.java;

public class Ejercicio3 {

	public static void main(String[] args) {
		Node a = new Node(1);
		Node b = new Node(7);
		Node c = new Node(12);
		Node d = new Node(9);
		Node e = new Node(6);

		a.next = b;
		b.previous = a;
		b.next = c;
		c.previous = b;
		c.next = d;
		d.previous = c;
		d.next = e;
		e.previous = d;
		
		System.out.println(countElements(d));

		/* Dada un nodo cualquiera en una lista doblemente
		 * enlazada, explicar c�mo obtener la cantidad total de elementos en esa lista.
		 * Los �nicos m�todos que expone el tipo nodo son getPrevious() y getNext() que
		 * devuelven null si no hay anterior/pr�ximo y getData() que devuelve el dato
		 * del nodo.
		 */
	}

	public static int countElements(Node node){
	
	if (node == null) {
		return 0;
	}
	Node auxNode = node;
	int count = 1;
	
	while (auxNode.getNext() != null) {
		count ++;
		auxNode = auxNode.getNext();
	}
	
	auxNode = node;
	
	while (auxNode.getPrevious() != null) {
		count ++;
		auxNode = auxNode.getPrevious();
	}
	
		return count;
	}

	static class Node {
		int data;
		Node previous;
		Node next;

		public Node(int value) {
			this.previous = null;
			this.next = null;
			this.data = value;
		}

		public Node getNext() {
			return this.next;
		}

		public Node getPrevious() {
			return this.previous;
		}

		public int getData() {
			return this.data;
		}
	}

}
