package com.citi.test.java;

public class Ejercicio8 {

	public static void main(String[] args) {
		char[][] matrix = { { ' ', ' ', ' ', ' ' }, { 'x', 'x', ' ', 'x' }, { ' ', ' ', ' ', ' ' },
				{ ' ', ' ', 'x', ' ' }, { ' ', ' ', ' ', ' ' }, };

		findPath(matrix);

	}

	public static void findPath(char[][] matrix) {
		int rows = matrix.length;
		int columns = matrix[0].length;

		boolean[][] visited = new boolean[rows][columns];
		if (existsValidPath(matrix, visited, 0, 0)) {
			for (int i = 0; i < visited.length; i++) {
				for (int j = 0; j < visited[0].length; j++) {
					if(visited[i][j]) {
						System.out.print("(" + i + "," + j + ") ");
					}
				}
			}
		} else {
			System.out.println("No existe un camino posible");
		}
	}

	private static boolean existsValidPath(char[][] matrix, boolean[][] visited, int i, int j) {
		if (i >= matrix.length || j >= matrix[0].length) {
			return true;
		}

		if(i < 0 || j < 0 || matrix[i][j] == 'x' || visited[i][j] ) {
			return false;
		}
    	
		visited[i][j] = true;
		
		if (existsValidPath(matrix, visited, i+1, j) || 
			existsValidPath(matrix, visited, i-1, j) ||
			existsValidPath(matrix, visited, i, j+1) || 
			existsValidPath(matrix, visited, i, j-1)) {
			return true;
		}
		
		visited[i][j] = false;
		
		return false;
	}

}
