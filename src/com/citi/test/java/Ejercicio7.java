package com.citi.test.java;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Ejercicio7 {

	public static void main(String[] args) throws IOException {

		FileInputStream inputStream = new FileInputStream("salesData1.txt");
		FileOutputStream outputStream = new FileOutputStream("salesData2.txt", true);

		Scanner sc = new Scanner(inputStream, "UTF-8");

		String oldSeller = "";
		String currentSeller = "";
		int currentQuantity = 0;
		int totalSales = 0;
		List<String> currentSellerData = new ArrayList<String>();

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			currentSeller = line.substring(0, line.indexOf(" "));
			currentQuantity = Integer.parseInt(line.substring(line.lastIndexOf(" ") + 1));

			if (currentSeller.equals(oldSeller)) {
				totalSales += currentQuantity;
				currentSellerData.add(line);

			} else {
				int auxTotalSales = totalSales;
				currentSellerData = writeFile(outputStream, currentSellerData, auxTotalSales);

				currentSellerData.clear();
				currentSellerData.add(line);

				oldSeller = currentSeller;
				totalSales = currentQuantity;

				if (!sc.hasNextLine()) {
					currentSellerData = writeFile(outputStream, currentSellerData, auxTotalSales);

				}
			}
		}

		if (inputStream != null) {
			inputStream.close();
		}
		if (sc != null) {
			sc.close();
		}
	}

	private static List<String> writeFile(FileOutputStream outputStream, List<String> currentSellerData,
			int auxTotalSales) {
		currentSellerData = currentSellerData.stream().map((l -> l.concat(" " + auxTotalSales + "\n")))
				.collect(Collectors.toList());

		currentSellerData.forEach(l -> {
			try {
				outputStream.write(l.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
		return currentSellerData;
	}
}
