package com.citi.test.java;
import java.util.Arrays;
import java.util.List;

public class Ejercicio1 {

	public static void main(String[] args) {

		List<Integer> array = Arrays.asList(5, 8, 0, -10, 44, 89, 1, 3, 7, 77, 12, -3, 4);
	
		// version 1
		Integer maxValue = array.stream().max(Integer::compare).get();
		System.out.println(maxValue);
		
		// version 2
		System.out.println(getMax(array));
	}

	private static int getMax(List<Integer> array) {
		if (array == null || array.size() == 0) {
			return -1;
		}
		
		int max = array.get(0);
		for (int i=1; i<array.size(); i++) {
				max = Math.max(max, array.get(i));   
		}
	
		return max;
	}
	
}
