package com.citi.test.java;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ejercicio6 {

	public static void main(String[] args) {

		try {
			String fileName1 = "contratos1.txt";
			String fileName2 = "contratos2.txt";
			String fileName3 = "contratos3.txt";
			List<String> newFileContent = new ArrayList<String>();

			List<String> fileContent1 = Files.lines(Paths.get(fileName1)).collect(Collectors.toList());
			List<String> fileContent2 = Files.lines(Paths.get(fileName2)).collect(Collectors.toList());

			fileContent2.forEach(line -> newFileContent.add(line));
			List<String> contractNumbersToChange = Files.lines(Paths.get(fileName2)).
												 	map(line -> getContractNumber(line))
												 	.distinct()
												 	.collect(Collectors.toList());
			
			fileContent1.forEach(line -> {
				if(!contractNumbersToChange.contains(getContractNumber(line))) {
					newFileContent.add(line);
				}
			});
			
			Collections.sort(newFileContent);
			Files.write(Paths.get(fileName3), newFileContent);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String getContractNumber(String line){
		return line.substring(0, line.indexOf(" "));
	}

}
