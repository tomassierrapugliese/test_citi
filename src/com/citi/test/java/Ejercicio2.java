package com.citi.test.java;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Ejercicio2 {

	public static void main(String[] args) {
		rangesSum(Arrays.asList(6, 7, 5, 4, 3, 1, 2, 3, 5, 6, 7, 9, 0, 0, 1, 2, 4, 1, 2, 3, 5, 1, 2));
	}

	public static Map<Integer, Integer> rangesSum(List<Integer> array) {
		if (array.size() <= 1) {
			return new TreeMap<Integer, Integer>();
		}

		int left = 0;
		int right = 1;
		Map<Integer, Integer> result = new TreeMap<Integer, Integer>();

		while (right < array.size()) {
			int sliceSum = array.subList(left, right+1).stream().mapToInt(Integer::intValue).sum();

			if (sliceSum < 13) {
				right++;

			} else {
				if (sliceSum == 13) {
					result.put(left, right);
				}
				left++;
				right = left +1;
			}
		}

		for (Integer entry : result.keySet()) {
			System.out.println("intervalo (" + entry + "," + result.get(entry) + ") ");
		}

		return result;
	}
}
